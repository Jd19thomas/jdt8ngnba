/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdt8ngnba;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author josh
 */
public abstract class jdt8ngRequest implements Serializable {
    
    public int status;
    public HttpURLConnection con;
    public BufferedReader reader;
    public String line;
    public String response;
    public Boolean showAlertTwo = false;
    public StringBuffer responseContent = new StringBuffer();
    
    public void makeConnection(String url) throws MalformedURLException, IOException {
        URL tempUrl = new URL(url);
        con = (HttpURLConnection) tempUrl.openConnection();
        con.setRequestMethod("GET");
        con.setConnectTimeout(5000);
        con.setReadTimeout(5000);
        
        status = con.getResponseCode();
    }
    
    public void getResponse() throws IOException {
        System.out.println("" + status);
        
        if(status > 299) {
            showAlertTwo = true;
            reader = new BufferedReader(new InputStreamReader(con.getErrorStream()));
            while((line = reader.readLine()) != null) {
                responseContent.append(line);
            }
            
            reader.close ();           
        } else {
            
            showAlertTwo = false;
            reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            while((line = reader.readLine()) != null) {
                responseContent.append(line);
            }
        
            
            reader.close();
            response = responseContent.toString(); 
            System.out.println("response:" + response);
            
        }
        
        con.disconnect();
        con = null;
        reader = null;
    }
       
}
