/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdt8ngnba;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author josh
 */
public class jdt8ngPoints extends jdt8ngRequest implements jd8ngStatInterface, Serializable{
    
    double stat;
    ArrayList<Integer> seasons = new ArrayList<Integer>();
    ArrayList<Double> statsForSeason = new ArrayList<Double>();
    int id;

    @Override
    public void getStat(int id) {
        this.id = id;
        String temp = "https://www.balldontlie.io/api/v1/season_averages?player_ids[]=" + id;
        try {
            makeConnection(temp);
            getResponse();
            System.out.println(response);
            
            JSONObject resp = new JSONObject(response);
            JSONArray data = resp.getJSONArray("data");
            JSONObject averages = data.getJSONObject(0);
           
            stat = averages.getDouble("pts");
            
        } catch (IOException ex) {
            Logger.getLogger(jdt8ngPoints.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void setSeasons(int start, int end) {
        for(int i = start; i <= end; i++) {
            String temp = "https://www.balldontlie.io/api/v1/season_averages?season=" + i + "&player_ids[]=" + id;
            try {
                
                jdt8ngPoints tempPoints = new jdt8ngPoints();
                tempPoints.makeConnection(temp);
                tempPoints.getResponse();
                
//                System.out.println("Response " + tempPoints.response);
                JSONObject resp = new JSONObject(tempPoints.response);
                
                JSONArray data = resp.getJSONArray("data");
                if(data.isEmpty()) {
                    System.out.println("This " + i + " is not a valid season");
                    statsForSeason.add(null);
                    seasons.add(i);
                } else {
                    JSONObject averages = data.getJSONObject(0);
           
                    double tempStat = averages.getDouble("pts");
                
                    System.out.println("setseaons " + i + " " + tempStat);
                    statsForSeason.add(tempStat);
                    seasons.add(i);
                }
//                seasons.add(i);
                
                
            } catch (IOException ex) {
                Logger.getLogger(jdt8ngPoints.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }

}
