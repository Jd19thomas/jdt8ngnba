/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdt8ngnba;




import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author josh
 */
public class jdt8ngPlayerModel extends jdt8ngRequest implements Serializable {
    
   
    public BufferedReader reader;
    public String line;
    public StringBuffer responseContent = new StringBuffer();
    public String firstNameInput;
    public String lastNameInput;
    public Boolean foundPlayer = false;
    public jdt8ngPoints Points = new jdt8ngPoints();
    public int startSeason;
    public int endSeason;
    public Boolean showAlert = false;
    
    
    public String firstName;
    public String lastName;
    public int id;
    public double pointAvg;
    public ArrayList<Integer> seasons = new ArrayList<Integer>();
    public ArrayList<Double> pointsForSeason = new ArrayList<Double>();
    
    public jdt8ngPlayerModel(String firstName, String lastName) {
        firstNameInput = firstName;
        lastNameInput = lastName;
    }
    
    public void findPlayer() {
        parseJSON();
    }
    
    
    public void parseJSON() {
       String temp = "https://www.balldontlie.io/api/v1/players?search=" + firstNameInput + "_" + lastNameInput;
        try {
            makeConnection(temp);
            getResponse();
//            System.out.println(response);
            if(showAlertTwo == true ) {
              showAlert = true;
              return;
            }
            
            JSONObject resp = new JSONObject(response);
            JSONArray data = resp.getJSONArray("data");
            if(data.isEmpty()) {
               showAlert = true;
               return;
            }
            JSONObject personInfo = data.getJSONObject(0);
//            System.out.println("person_info" + personInfo.toString());

            firstName = personInfo.getString("first_name");
            lastName = personInfo.getString("last_name");
            id = personInfo.getInt("id");
            foundPlayer = true;
            Points.getStat(id);
            pointAvg = Points.stat;
            
        } catch (IOException ex) {
            showAlert = true;
            ex.printStackTrace();
        } 
        
    }
    
   
    
    public void getStats(int start, int end) {
        startSeason = start;
        endSeason = end;
        Points.setSeasons(start, end);
        seasons = Points.seasons;
        pointsForSeason = Points.statsForSeason;
    }
    
    
}
