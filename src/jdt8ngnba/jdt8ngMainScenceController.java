/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdt8ngnba;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author josh
 */
public class jdt8ngMainScenceController implements Initializable {

    
    private FXMLDocumentController FXMLDocumentController;
    private AboutFXMLController aboutFXMLController;
    private Scene about;
    private Scene individualPlayer;
    private Scene main;
    private Stage stage;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public void start(Stage stage){
        this.stage = stage; 
        main = stage.getScene(); 
        System.out.println("Page 1 has started"); 
    }
    
    @FXML
    private void goToPlayerPage(ActionEvent event) {
        System.out.println("Going to page 2"); 
        
        try {
            if(individualPlayer == null){
                System.out.println("individualPlayer is null"); 
                
                FXMLLoader loader = new FXMLLoader(getClass().getResource("FXMLDocument.fxml"));
                Parent individualPlayerRoot = loader.load(); 
                FXMLDocumentController = loader.getController(); 
                FXMLDocumentController.main = main; 
                FXMLDocumentController.jdt8ngMainScenceController = this; 
                individualPlayer = new Scene(individualPlayerRoot); 
            }
        } catch (Exception ex){
            
        }
        
        stage.setScene(individualPlayer); 
        FXMLDocumentController.start(stage);
       
    }
    
    @FXML
    private void goToAboutPage(ActionEvent event) {
        System.out.println("Going to page 2"); 
        
        try {
            if(about == null){
                System.out.println("individualPlayer is null"); 
                
                FXMLLoader loader = new FXMLLoader(getClass().getResource("AboutFXML.fxml"));
                Parent aboutRoot = loader.load(); 
                aboutFXMLController = loader.getController(); 
                aboutFXMLController.main = main; 
                aboutFXMLController.jdt8ngMainScenceController = this; 
                about = new Scene(aboutRoot); 
            }
        } catch (Exception ex){
            
        }
        
        stage.setScene(about); 
        aboutFXMLController.start(stage);
       
    }
}
    

