/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdt8ngnba;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author josh
 */
public class AboutFXMLController implements Initializable {

    
    public Scene main;
    public Stage stage;
    public jdt8ngMainScenceController jdt8ngMainScenceController;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    public void goBackToMain(ActionEvent event) {
        stage.setScene(main); 
    }
    
    @FXML
    public void start(Stage stage){
        this.stage = stage; 
        System.out.println("about me has started"); 
    }
    
}
