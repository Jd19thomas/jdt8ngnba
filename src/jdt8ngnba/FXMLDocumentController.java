/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdt8ngnba;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author josh
 */
public class FXMLDocumentController implements Initializable {
    
    public jdt8ngPlayerModel player; 
    
    @FXML
    public TextField firstName;
    public TextField lastName;
    public TextField startSeason;
    public TextField endSeason;
    public LineChart lineChart;
    public Scene main;
    public Stage stage;
    public jdt8ngMainScenceController jdt8ngMainScenceController;
    public Label fName;
    public Label lName;
    public Label points;
    public CategoryAxis xAxis;
    public NumberAxis yAxis;
    public XYChart.Series<String, Number> series = new XYChart.Series();
    
    
    @FXML
    private void handleButtonAction(ActionEvent event) throws ProtocolException, IOException {
       if(firstName.getText().isEmpty() || lastName.getText().isEmpty()) {
           Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("please enter both last name and first name");
            a.show();
            return;
       }
        
        player = new jdt8ngPlayerModel(firstName.getText(), lastName.getText());
        player.findPlayer();
        
        if(player.showAlert) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("Player is currently injured, no longer in the nba, or the name is invalid");
            a.show();
            return;
        }
    
        
        fName.setText("First Name: " + player.firstName);
        lName.setText("Last Name: " + player.lastName);
        points.setText("Points Per Game: " +player.pointAvg);
        
    }
    
    @FXML
    private void setGraph(ActionEvent event) {
        if(firstName.getText().isEmpty() || lastName.getText().isEmpty()) {
           Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("please enter both last name and first name");
            a.show();
            return;
       }
        
        player = new jdt8ngPlayerModel(firstName.getText(), lastName.getText());
        player.findPlayer();
        
        if(player.showAlert) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("Player is currently injured, no longer in the nba, or the name is invalid");
            a.show();
            return;
        }
        series.getData().clear();
        lineChart.getData().remove(series);
        lineChart.getData().add(series);
        lineChart.setAnimated(false);
        if(player.foundPlayer) {
            player.getStats(Integer.parseInt(startSeason.getText()), Integer.parseInt(endSeason.getText()));
            for(int i= 0; i<(player.seasons).size(); i++) {
                if(player.pointsForSeason.get(i) != null) {
                   series.getData().add(new XYChart.Data(Integer.toString(player.seasons.get(i)), player.pointsForSeason.get(i)));
                }
            }
        }
    }
    
    @FXML
    public void fav(ActionEvent event) {
        if(player.foundPlayer == false){
            return; 
        }
        if(player == null) {
            return;
        }
        
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showSaveDialog(stage);
        if (file != null) {
            
            try {
                FileOutputStream fileOut = new FileOutputStream(file.getPath());
                ObjectOutputStream out = new ObjectOutputStream(fileOut); 
                
                out.writeObject(player);
                out.close(); 
                fileOut.close(); 
                
            } catch (FileNotFoundException ex) {
                String message = "File not found exception occured while saving to " + file.getPath(); 
                System.out.println(message);
//                displayExceptionAlert(message, ex); 
                
            } catch (IOException ex) {
                String message = "IOException occured while saving to " + file.getPath();
                ex.printStackTrace();
                System.out.println(message);
//                displayExceptionAlert(message, ex);
                
            }
        }      
    }
    
    @FXML
    public void loadFav(ActionEvent event) {
        System.out.println("hi");
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(stage);
        System.out.println("bar");
        if (file != null) {
            System.out.println("foo");
            FileInputStream fileIn; 
            try {
                 
                System.out.println("bar2");
                fileIn = new FileInputStream(file.getPath());
                
                ObjectInputStream in = new ObjectInputStream(fileIn); 
                
                System.out.println("hi2");
                player = (jdt8ngPlayerModel)in.readObject();
                System.out.println("hi3");
                System.out.println("First name is " + player.firstName);
                in.close(); 
                fileIn.close(); 
                fillWithPlayer(player);
                
                
            } catch (FileNotFoundException ex) {
                String message = "File not found exception occured while opening " + file.getPath(); 
                System.out.println(message);
//                displayExceptionAlert(message, ex); 
                
            } catch (IOException ex) {
                String message = "IO exception occured while opening " + file.getPath(); 
                System.out.println(message);
//                displayExceptionAlert(message, ex);
                
            } catch (ClassNotFoundException ex) {
                String message = "Class not found exception occured while opening " + file.getPath(); 
                System.out.println(message);
//                displayExceptionAlert(message, ex); 
            }
            
            
            
        }
    }
    
    public void fillWithPlayer(jdt8ngPlayerModel player) {
        firstName.setText("foo");
        lastName.setText(player.lastName);
        series.getData().clear();
        lineChart.getData().remove(series);
        lineChart.getData().add(series);
        lineChart.setAnimated(false);
        for(int i= 0; i<(player.seasons).size(); i++) {
            if(player.pointsForSeason.get(i) != null) {
                series.getData().add(new XYChart.Data(Integer.toString(player.seasons.get(i)), player.pointsForSeason.get(i)));
            }
        }
        
                
    }
    
    @FXML
    private void goBackToMain(ActionEvent event) {
        stage.setScene(main); 
    }
    
    @FXML
    public void start(Stage stage){
        this.stage = stage; 
        System.out.println("Page 2 has started"); 
    }
    
    public void checkalert() {
        if(firstName.getText().isEmpty() || lastName.getText().isEmpty()) {
           Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("please enter both last name and first name");
            a.show();
            return;
       }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
    }    
    
}
